<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Accessor;

use JSONAPI\Expression\Accessor;
use JSONAPI\Expression\Field\AbstractProperty;

/**
 * Class SimpleAccessor
 *
 * @package JSONAPI\Expression\Accessor
 */
class SimpleAccessor implements Accessor
{
    /**
     * @inheritDoc
     */
    public function __invoke(AbstractProperty $field, array $args = []): string
    {
        return $field->getName();
    }
}
