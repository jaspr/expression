<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use JSONAPI\Expression\Accessor\ObjectPropertyAccessor;
use JSONAPI\Expression\Accessor\SimpleAccessor;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Field\AbstractProperty;

/**
 * Interface Accessor
 * Use this for accessing to Field, parse field string by yourself.
 * Can serve for requiring joins for SQL, or traversing trough objects
 *
 * @see ObjectPropertyAccessor
 * @see SimpleAccessor
 *
 * @package JSONAPI\Expression
 */
interface Accessor
{
    /**
     * The __invoke method is called when a script tries to call an object as a function.
     *
     * @param AbstractProperty $field
     * @param array            $args
     *
     * @return mixed
     * @throws ExpressionError
     * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
     */
    public function __invoke(AbstractProperty $field, array $args = []): mixed;
}
