<?php

/**
 * Created by tomas
 * 13.06.2022 21:34
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use DateTimeInterface;
use JSONAPI\Expression\Comparability\ArrayCompare;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;

class ArrayProperty extends AbstractProperty implements TArray
{
    use ArrayCompare;

    /**
     * @var string type
     */
    private string $type;

    public function __construct(string $name, string $type)
    {
        parent::__construct($name);
        $this->type = $this->translateType($type);
    }

    private function translateType(string $type): string
    {
        return match ($type) {
            'string'                 => TString::class,
            'integer', 'double'      => TNumeric::class,
            'boolean'                => TBoolean::class,
            'array'                  => TArray::class,
            DateTimeInterface::class => TDateTime::class,
            'NULL'                   => 'NULL'
        };
    }
}
