<?php

/**
 * Created by tomas
 * 13.06.2022 21:28
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use JSONAPI\Expression\Comparability\BooleanCompare;
use JSONAPI\Expression\Type\TBoolean;

class BooleanProperty extends AbstractProperty implements TBoolean
{
    use BooleanCompare;
}
