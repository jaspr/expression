<?php

/**
 * Created by tomas
 * 13.06.2022 21:28
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use JSONAPI\Expression\Comparability\DateTimeCompare;
use JSONAPI\Expression\Type\TDateTime;

class DateTimeProperty extends AbstractProperty implements TDateTime
{
    use DateTimeCompare;
}
