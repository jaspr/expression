<?php

/**
 * Created by tomas
 * 13.06.2022 21:21
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use JSONAPI\Expression\Comparability\StringCompare;
use JSONAPI\Expression\Type\TString;

class StringProperty extends AbstractProperty implements TString
{
    use StringCompare;
}
