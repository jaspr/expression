<?php

/**
 * Created by tomas
 * 13.06.2022 21:28
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use JSONAPI\Expression\Comparability\NumericCompare;
use JSONAPI\Expression\Type\TNumeric;

class NumericProperty extends AbstractProperty implements TNumeric
{
    use NumericCompare;
}
