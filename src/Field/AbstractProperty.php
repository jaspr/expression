<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Field;

use JSONAPI\Expression\Dispatcher;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Expression;

/**
 * Class Field
 *
 * @package JSONAPI\Expression\Expression
 */
abstract class AbstractProperty implements Expression
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
