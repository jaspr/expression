<?php

/**
 * Created by tomas
 * 13.06.2022 22:17
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Comparability;

use JSONAPI\Expression\Expression;

trait ArrayCompare
{
    public function isComparable(Expression $expression): bool
    {
        if ($this->type == 'NULL') {
            return true;
        }
        return $expression instanceof $this->type;
    }
}
