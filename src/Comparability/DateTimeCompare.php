<?php

/**
 * Created by tomas
 * 13.06.2022 22:16
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Comparability;

use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TDateTime;

trait DateTimeCompare
{
    public function isComparable(Expression $expression): bool
    {
        if ($expression instanceof TArray) {
            return $expression->isComparable($this);
        }
        return $expression instanceof TDateTime;
    }
}
