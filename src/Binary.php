<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

/**
 * Interface Binary
 *
 * @package JSONAPI\Expression\Expression
 */
interface Binary extends Expression
{
    /**
     * @return Expression
     */
    public function getLeft(): Expression;

    /**
     * @return Expression
     */
    public function getRight(): Expression;

    /**
     * @return Operator
     */
    public function getOp(): Operator;
}
