<?php
// phpcs:ignoreFile -- cannot parse enum right
declare(strict_types=1);

namespace JSONAPI\Expression;

/**
 * Class Operator
 *
 * @package JSONAPI\Expression\Expression
 */
enum Operator
{
    // LOGICAL
    case LOGICAL_EQUAL;
    case LOGICAL_NOT_EQUAL;
    case LOGICAL_GREATER_THAN;
    case LOGICAL_GREATER_THAN_OR_EQUAL;
    case LOGICAL_LOWER_THAN;
    case LOGICAL_LOWER_THAN_OR_EQUAL;
    case LOGICAL_AND;
    case LOGICAL_OR;
    case LOGICAL_IN;
    case LOGICAL_HAS;
    case LOGICAL_BETWEEN;

    // ARITHMETIC
    case ARITHMETIC_ADDITION;
    case ARITHMETIC_SUBTRACTION;
    case ARITHMETIC_NEGATION;
    case ARITHMETIC_MULTIPLICATION;
    case ARITHMETIC_DIVISION;
    case ARITHMETIC_MODULO;

    // FUNCTION
    /* String */
    case FUNCTION_NOT;
    case FUNCTION_STARTS_WITH;
    case FUNCTION_ENDS_WITH;
    case FUNCTION_CONTAINS;
    case FUNCTION_CONCAT;
    case FUNCTION_INDEX_OF;
    case FUNCTION_LENGTH;
    case FUNCTION_SUBSTRING;
    case FUNCTION_MATCHES_PATTERN;
    case FUNCTION_TO_LOWER;
    case FUNCTION_TO_UPPER;
    case FUNCTION_TRIM;

    /* Date & time */
    case FUNCTION_DATE;
    case FUNCTION_DAY;
    case FUNCTION_HOUR;
    case FUNCTION_MINUTE;
    case FUNCTION_MONTH;
    case FUNCTION_SECOND;
    case FUNCTION_TIME;
    case FUNCTION_YEAR;

    /* Arithmetic */
    case FUNCTION_CEILING;
    case FUNCTION_FLOOR;
    case FUNCTION_ROUND;
}
