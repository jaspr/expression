<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

use RuntimeException;

/**
 * Class MissingDependency
 *
 * @package JSONAPI\Expression\Exception
 */
class MissingDependency extends RuntimeException
{
}
