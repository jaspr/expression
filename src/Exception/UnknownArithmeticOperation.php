<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

/**
 * Class UnknownArithmeticOperation
 *
 * @package JSONAPI\Expression\Exception
 */
class UnknownArithmeticOperation extends ExpressionError
{
    public function __construct($operator)
    {
        parent::__construct(sprintf('Unknown arithmetic operation %s', $operator), 5004);
    }
}
