<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

use JSONAPI\Expression\Operator;

/**
 * Class UnknownOperator
 *
 * @package JSONAPI\Expression\Exception
 */
class UnknownComparisonOperator extends ExpressionError
{
    public function __construct(Operator $operator)
    {
        parent::__construct(sprintf('Unknown operator %s', $operator->name), 5002);
    }
}
