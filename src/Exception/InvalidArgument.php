<?php

/**
 * Created by uzivatel
 * at 24.03.2022 10:52
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

/**
 * Class InvalidArgument
 *
 * @package JSONAPI\Expression\Exception
 */
class InvalidArgument extends ExpressionError
{
}
