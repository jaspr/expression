<?php

/**
 * Created by tomas
 * 13.06.2022 22:24
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

use JSONAPI\Expression\Expression;

class IncomparableExpressions extends ExpressionError
{
    public function __construct(Expression $left, Expression $right)
    {
        $message = "Left expression of type: %s is not comparable with right expression ot type: %s .";
        parent::__construct(sprintf($message, get_class($left), get_class($right)));
    }
}
