<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

/**
 * Class UnknownFunction
 *
 * @package JSONAPI\Expression\Exception
 */
class UnknownFunction extends ExpressionError
{
    public function __construct($fn)
    {
        parent::__construct(sprintf('Unknown function %s', $fn), 5003);
    }
}
