<?php

/**
 * Created by tomas
 * 13.06.2022 22:57
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

class HeterogeneousArrayError extends InvalidArgument
{
}
