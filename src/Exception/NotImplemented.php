<?php

/**
 * Created by uzivatel
 * at 23.03.2022 12:38
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

use JSONAPI\Expression\Operator;

/**
 * Class NotImplemented
 *
 * @package JSONAPI\Expression\Exception
 */
class NotImplemented extends ExpressionError
{
    public function __construct(Operator $operator)
    {
        parent::__construct(sprintf('Operator %s is not implemented.', $operator->name), 5006);
    }
}
