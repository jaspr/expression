<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

/**
 * Class UnknownExpression
 *
 * @package JSONAPI\Expression\Exception
 */
class UnknownExpression extends ExpressionError
{
    public function __construct($className)
    {
        parent::__construct(sprintf('Unknown expression %s', $className), 5001);
    }
}
