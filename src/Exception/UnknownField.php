<?php

/**
 * Created by tomas
 * at 19.02.2021 19:56
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

/**
 * Class UnknownField
 *
 * @package JSONAPI\Expression\Exception
 */
class UnknownField extends ExpressionError
{
    public function __construct($object, $field)
    {
        parent::__construct(sprintf('Unknown field %s of object %s', $field, get_class($object)), 5005);
    }
}
