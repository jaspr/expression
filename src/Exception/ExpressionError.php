<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Exception;

use RuntimeException;

/**
 * Class JSONAPI\ExpressionError
 *
 * @package JSONAPI\Expression\Exception
 */
abstract class ExpressionError extends RuntimeException
{
    protected $code = 5000;
}
