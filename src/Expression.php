<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use JSONAPI\Expression\Exception\ExpressionError;

/**
 * Interface Expression
 *
 * @package JSONAPI\Expression
 */
interface Expression
{
    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function resolve(Dispatcher $dispatcher): mixed;

    public function isComparable(Expression $expression): bool;
}
