<?php

/**
 * Created by tomas
 * 13.06.2022 21:34
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use JSONAPI\Expression\Comparability\StringCompare;
use JSONAPI\Expression\Type\TString;

class StringValue extends AbstractValue implements TString
{
    use StringCompare;

    public function __construct(string $value)
    {
        $this->value = $value;
    }
}
