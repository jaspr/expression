<?php

/**
 * Created by tomas
 * 13.06.2022 21:33
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use DateTimeInterface;
use JSONAPI\Expression\Comparability\DateTimeCompare;
use JSONAPI\Expression\Type\TDateTime;

class DateTimeValue extends AbstractValue implements TDateTime
{
    use DateTimeCompare;

    public function __construct(DateTimeInterface $dateTime)
    {
        $this->value = $dateTime;
    }
}
