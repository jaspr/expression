<?php

/**
 * Created by tomas
 * 13.06.2022 22:42
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;

class NullValue extends AbstractValue implements TArray, TBoolean, TDateTime, TNumeric, TString
{
    public function __construct()
    {
        $this->value = null;
    }

    public function isComparable(Expression $expression): bool
    {
        return $expression instanceof TArray ||
               $expression instanceof TBoolean ||
               $expression instanceof TDateTime ||
               $expression instanceof TNumeric ||
               $expression instanceof TString;
    }
}
