<?php

/**
 * Created by tomas
 * 13.06.2022 21:32
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use JSONAPI\Expression\Comparability\BooleanCompare;
use JSONAPI\Expression\Type\TBoolean;

class BooleanValue extends AbstractValue implements TBoolean
{
    use BooleanCompare;

    public function __construct(bool $value)
    {
        $this->value = $value;
    }
}
