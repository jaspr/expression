<?php

/**
 * Created by tomas
 * 13.06.2022 21:34
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use DateTimeInterface;
use JSONAPI\Expression\Exception\HeterogeneousArrayError;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Comparability\ArrayCompare;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;

/**
 * Class ArrayValue
 *
 * @package JSONAPI\Expression\Literal
 */
class ArrayValue extends AbstractValue implements TArray
{
    use ArrayCompare;

    private string $type;

    /**
     * @throws HeterogeneousArrayError
     */
    public function __construct(array $value)
    {
        $arr = [];
        if (empty($value)) {
            $this->type = 'NULL';
        } else {
            foreach ($value as $item) {
                $type = $this->translateType($item);
                if (isset($this->type) && ($this->type != $type)) {
                    throw new HeterogeneousArrayError();
                } else {
                    $this->type = $type;
                }
                if (!($item instanceof Expression)) {
                    $arr[] = match ($type) {
                        TString::class => new StringValue($item),
                        TNumeric::class => new NumericValue($item),
                        TBoolean::class => new BooleanValue($item),
                        TArray::class => new ArrayValue($item),
                        TDateTime::class => new DateTimeValue($item),
                        'NULL' => new NullValue(),
                    };
                } else {
                    $arr[] = $item;
                }
            }
        }
        $this->value = $arr;
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    private function translateType(mixed $value): string
    {
        if ($value instanceof TString) {
            return TString::class;
        }
        if ($value instanceof TNumeric) {
            return TNumeric::class;
        }
        if ($value instanceof TBoolean) {
            return TBoolean::class;
        }
        if ($value instanceof TArray) {
            return TArray::class;
        }
        if ($value instanceof DateTimeInterface || $value instanceof TDateTime) {
            return TDateTime::class;
        }
        return match (gettype($value)) {
            'string' => TString::class,
            'integer', 'double' => TNumeric::class,
            'boolean' => TBoolean::class,
            'array' => TArray::class,
            'NULL' => 'NULL'
        };
    }
}
