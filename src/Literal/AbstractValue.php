<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use JSONAPI\Expression\Dispatcher;
use JSONAPI\Expression\Expression;

/**
 * Class Literal
 *
 * @package JSONAPI\Expression\Expression
 */
abstract class AbstractValue implements Expression
{
    /** @var mixed */
    protected mixed $value;

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
