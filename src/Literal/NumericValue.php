<?php

/**
 * Created by tomas
 * 13.06.2022 21:33
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Literal;

use JSONAPI\Expression\Comparability\NumericCompare;
use JSONAPI\Expression\Type\TNumeric;

class NumericValue extends AbstractValue implements TNumeric
{
    use NumericCompare;

    public function __construct(int|float $value)
    {
        $this->value = $value;
    }
}
