<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use DateTime;
use DateTimeInterface;
use JSONAPI\Expression\Exception\IncomparableExpressions;
use JSONAPI\Expression\Exception\InvalidArgument;
use JSONAPI\Expression\Field\ArrayProperty;
use JSONAPI\Expression\Field\BooleanProperty;
use JSONAPI\Expression\Field\DateTimeProperty;
use JSONAPI\Expression\Field\NumericProperty;
use JSONAPI\Expression\Field\StringProperty;
use JSONAPI\Expression\Fn\BooleanMethod;
use JSONAPI\Expression\Fn\DateTimeMethod;
use JSONAPI\Expression\Fn\NumericMethod;
use JSONAPI\Expression\Fn\StringMethod;
use JSONAPI\Expression\Literal\ArrayValue;
use JSONAPI\Expression\Literal\BooleanValue;
use JSONAPI\Expression\Literal\DateTimeValue;
use JSONAPI\Expression\Literal\NullValue;
use JSONAPI\Expression\Literal\NumericValue;
use JSONAPI\Expression\Literal\StringValue;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;

/**
 * Class Ex - shortcut to build expressions and create expression tree
 *
 * @package JSONAPI\Expression
 */
final class Ex
{
    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function eq(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_EQUAL, $right);
    }

    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function ne(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_NOT_EQUAL, $right);
    }

    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function lt(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_LOWER_THAN, $right);
    }

    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function le(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_LOWER_THAN_OR_EQUAL, $right);
    }

    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function gt(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_GREATER_THAN, $right);
    }

    /**
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $left
     * @param TArray|TBoolean|TDateTime|TNumeric|TString $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function ge(
        TArray|TBoolean|TDateTime|TNumeric|TString $left,
        TArray|TBoolean|TDateTime|TNumeric|TString $right
    ): TBoolean {
        return new Comparison($left, Operator::LOGICAL_GREATER_THAN_OR_EQUAL, $right);
    }

    /**
     * @param TNumeric|TString $left
     * @param TArray $right
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function in(TNumeric|TString $left, TArray $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_IN, $right);
    }

    /**
     * @param TArray $right
     * @param TString|TNumeric $left
     *
     * @return TBoolean
     * @throws IncomparableExpressions
     */
    public static function has(TArray $right, TString|TNumeric $left): TBoolean
    {
        return new Comparison($right, Operator::LOGICAL_HAS, $left);
    }

    /**
     * @param TNumeric|TDateTime $left
     * @param TNumeric|TDateTime $from
     * @param TNumeric|TDateTime $to
     *
     * @return TBoolean
     * @throws Exception\HeterogeneousArrayError
     * @throws IncomparableExpressions
     */
    public static function be(TNumeric|TDateTime $left, TNumeric|TDateTime $from, TNumeric|TDateTime $to): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_BETWEEN, new ArrayValue([$from, $to]));
    }

    /**
     * @param TBoolean $left
     * @param TBoolean $right
     *
     * @return TBoolean
     */
    public static function and(TBoolean $left, TBoolean $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_AND, $right);
    }

    /**
     * @param TBoolean $left
     * @param TBoolean $right
     *
     * @return TBoolean
     */
    public static function or(TBoolean $left, TBoolean $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_OR, $right);
    }

    /**
     * @param TBoolean $expression
     *
     * @return TBoolean
     */
    public static function not(TBoolean $expression): TBoolean
    {
        return new BooleanMethod(Operator::FUNCTION_NOT, [$expression]);
    }

    /**
     * @param TString $subject
     *
     * @return TNumeric
     */
    public static function length(TString $subject): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_LENGTH, [$subject]);
    }

    /**
     * @param TString $subject
     * @param TString $append
     *
     * @return TString
     */
    public static function concat(TString $subject, TString $append): TString
    {
        return new StringMethod(Operator::FUNCTION_CONCAT, [$subject, $append]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function contains(TString $haystack, TString $needle): TBoolean
    {
        return new BooleanMethod(Operator::FUNCTION_CONTAINS, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function startsWith(TString $haystack, TString $needle): TBoolean
    {
        return new BooleanMethod(Operator::FUNCTION_STARTS_WITH, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function endsWith(TString $haystack, TString $needle): TBoolean
    {
        return new BooleanMethod(Operator::FUNCTION_ENDS_WITH, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TNumeric
     */
    public static function indexOf(TString $haystack, TString $needle): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_INDEX_OF, [$haystack, $needle]);
    }

    /**
     * @param TString $string
     * @param TNumeric $start
     * @param TNumeric|null $length
     *
     * @return TString
     */
    public static function substring(TString $string, TNumeric $start, ?TNumeric $length = null): TString
    {
        if ($length) {
            return new StringMethod(Operator::FUNCTION_SUBSTRING, [$string, $start, $length]);
        }
        return new StringMethod(Operator::FUNCTION_SUBSTRING, [$string, $start]);
    }

    /**
     * @param TString $subject
     * @param TString $pattern
     *
     * @return TBoolean
     */
    public static function matchesPattern(TString $subject, TString $pattern): TBoolean
    {
        return new BooleanMethod(Operator::FUNCTION_MATCHES_PATTERN, [$pattern, $subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function toLower(TString $subject): TString
    {
        return new StringMethod(Operator::FUNCTION_TO_LOWER, [$subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function toUpper(TString $subject): TString
    {
        return new StringMethod(Operator::FUNCTION_TO_UPPER, [$subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function trim(TString $subject): TString
    {
        return new StringMethod(Operator::FUNCTION_TRIM, [$subject]);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function add(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_ADDITION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function sub(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_SUBTRACTION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function mul(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_MULTIPLICATION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function div(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_DIVISION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function mod(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_MODULO, $y);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function ceiling(TNumeric $value): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_CEILING, [$value]);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function floor(TNumeric $value): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_FLOOR, [$value]);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function round(TNumeric $value): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_ROUND, [$value]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TDateTime
     */
    public static function date(TDateTime $datetime): TDateTime
    {
        return new DateTimeMethod(Operator::FUNCTION_DATE, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function day(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_DAY, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function hour(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_HOUR, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function minute(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_MINUTE, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function month(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_MONTH, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function second(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_SECOND, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TDateTime
     */
    public static function time(TDateTime $datetime): TDateTime
    {
        return new DateTimeMethod(Operator::FUNCTION_TIME, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function year(TDateTime $datetime): TNumeric
    {
        return new NumericMethod(Operator::FUNCTION_YEAR, [$datetime]);
    }

    /**
     * @param string|int|float|bool|array|DateTimeInterface $value
     *
     * @return TArray|TBoolean|TDateTime|TNumeric|TString
     * @throws InvalidArgument
     */
    public static function literal(
        string|int|float|bool|array|DateTimeInterface|null $value
    ): TArray|TBoolean|TDateTime|TNumeric|TString {
        if ($value instanceof DateTimeInterface) {
            return new DateTimeValue($value);
        }
        return match (gettype($value)) {
            'string' => new StringValue($value),
            'integer', 'double' => new NumericValue($value),
            'boolean' => new BooleanValue($value),
            'array' => new ArrayValue($value),
            'NULL' => new NullValue()
        };
    }

    /**
     * @param string $name
     * @param string $type
     *
     * @return TArray|TBoolean|TDateTime|TNumeric|TString
     * @throws InvalidArgument
     */
    public static function field(string $name, string $type = 'string'): TArray|TBoolean|TDateTime|TNumeric|TString
    {
        if (enum_exists($type)) {
            return new StringProperty($name);
        }
        return match ($type) {
            'string' => new StringProperty($name),
            'string[]' => new ArrayProperty($name, 'string'),
            'integer', 'int', 'float', 'double' => new NumericProperty($name),
            'integer[]', 'int[]' => new ArrayProperty($name, 'integer'),
            'double[]', 'float[]' => new ArrayProperty($name, 'double'),
            'boolean', 'bool' => new BooleanProperty($name),
            'boolean[]', 'bool[]' => new ArrayProperty($name, 'boolean'),
            'datetime', DateTimeInterface::class, DateTime::class => new DateTimeProperty($name),
            'datetime[]', DateTimeInterface::class . '[]', DateTime::class . '[]' => new ArrayProperty(
                $name,
                DateTimeInterface::class
            ),
            'mixed' => new ArrayProperty($name, 'NULL'),
            default => throw new InvalidArgument("Unexpected property [$name] type [$type].")
        };
    }
}
