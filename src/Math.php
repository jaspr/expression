<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Comparability\NumericCompare;
use JSONAPI\Expression\Type\TNumeric;

/**
 * Class Math
 *
 * @package JSONAPI\Expression\Expression
 */
class Math implements Binary, TNumeric
{
    use NumericCompare;

    /**
     * @var TNumeric
     */
    private TNumeric $left;
    /**
     * @var TNumeric
     */
    private TNumeric $right;
    /**
     * @var Operator
     */
    private Operator $op;

    /**
     * Math constructor.
     *
     * @param TNumeric $left
     * @param Operator $operator
     * @param TNumeric $right
     */
    public function __construct(TNumeric $left, Operator $operator, TNumeric $right)
    {
        $this->left  = $left;
        $this->right = $right;
        $this->op    = $operator;
    }

    /**
     * @return TNumeric
     */
    public function getLeft(): TNumeric
    {
        return $this->left;
    }

    /**
     * @return TNumeric
     */
    public function getRight(): TNumeric
    {
        return $this->right;
    }

    /**
     * @return Operator
     */
    public function getOp(): Operator
    {
        return $this->op;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return float|int
     * @throws ExpressionError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
