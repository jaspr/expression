<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use JSONAPI\Expression\Exception\IncomparableExpressions;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Comparability\BooleanCompare;
use JSONAPI\Expression\Type\TBoolean;

/**
 * Class Comparison
 *
 * @package JSONAPI\Expression\Expression
 */
class Comparison implements Binary, TBoolean
{
    use BooleanCompare;

    /**
     * @var Expression
     */
    private Expression $left;
    /**
     * @var Operator
     */
    private Operator $op;
    /**
     * @var Expression
     */
    private Expression $right;

    /**
     * @param Expression $left
     * @param Operator   $operator
     * @param Expression $right
     *
     * @throws IncomparableExpressions
     */
    public function __construct(Expression $left, Operator $operator, Expression $right)
    {
        if ($left->isComparable($right)) {
            $this->left  = $left;
            $this->op    = $operator;
            $this->right = $right;
        } else {
            throw new IncomparableExpressions($left, $right);
        }
    }

    /**
     * @return Expression
     */
    public function getLeft(): Expression
    {
        return $this->left;
    }

    /**
     * @return Expression
     */
    public function getRight(): Expression
    {
        return $this->right;
    }

    /**
     * @return Operator
     */
    public function getOp(): Operator
    {
        return $this->op;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
