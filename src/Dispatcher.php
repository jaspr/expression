<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

use JSONAPI\Expression\Exception\ExpressionError;

/**
 * Interface Dispatcher
 *
 * @package JSONAPI\Expression
 */
interface Dispatcher
{
    /**
     * @param Expression $expression
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function dispatch(Expression $expression): mixed;
}
