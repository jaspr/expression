<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Dispatcher;

use JSONAPI\Expression\Exception\NotImplemented;
use JSONAPI\Expression\Exception\UnknownFunction;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Operator;

/**
 * Class SQLResolver
 *
 * @package JSONAPI\Expression\Dispatcher
 * @method string dispatch(Expression $expression)
 */
class SQLiteResolver extends SQLResolver
{
    /**
     * @inheritDoc
     */
    protected function resolveMethodConcat(array $args): string
    {
        return sprintf('CONCAT(%s,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodLength(array $args): string
    {
        return sprintf('LENGTH(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodIndexOf(array $args): string
    {
        return sprintf('INSTR(%s,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSubstring(array $args): string
    {
        if (count($args) == 3) {
            return sprintf('SUBSTR(%s,%s,%s)', ...$args);
        }
        return sprintf('SUBSTR(%s,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMatchesPattern(array $args): string
    {
        throw new UnknownFunction(Operator::FUNCTION_MATCHES_PATTERN);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDate(array $args): string
    {
        return sprintf('DATE(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTime(array $args): string
    {
        return sprintf('TIME(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDay(array $args): string
    {
        return sprintf('STRFTIME(%%d,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMonth(array $args): string
    {
        return sprintf('STRFTIME(%%m,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodYear(array $args): string
    {
        return sprintf('STRFTIME(%%Y,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodHour(array $args): string
    {
        return sprintf('STRFTIME(%%H,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMinute(array $args): string
    {
        return sprintf('STRFTIME(%%i,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSecond(array $args): string
    {
        return sprintf('STRFTIME(%%s,%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodFloor(array $args): string
    {
        throw new NotImplemented(Operator::FUNCTION_FLOOR);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodRound(array $args): string
    {
        return sprintf('ROUND(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodCeiling(array $args): string
    {
        throw new NotImplemented(Operator::FUNCTION_CEILING);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathAddition(mixed $left, mixed $right): string
    {
        return $left . ' + ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathSubtraction(mixed $left, mixed $right): string
    {
        return $left . ' - ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathModulo(mixed $left, mixed $right): string
    {
        return $left . ' % ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathDivision(mixed $left, mixed $right): string
    {
        return $left . ' / ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathMultiplication(mixed $left, mixed $right): string
    {
        return $left . ' * ' . $right;
    }
}
