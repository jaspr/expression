<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Dispatcher;

use JSONAPI\Expression\Accessor;
use JSONAPI\Expression\Accessor\SimpleAccessor;
use JSONAPI\Expression\Comparison;
use JSONAPI\Expression\Dispatcher;
use JSONAPI\Expression\Dispatcher\Domain\Arithmetics;
use JSONAPI\Expression\Dispatcher\Domain\Comparisons;
use JSONAPI\Expression\Dispatcher\Domain\Methods;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Exception\UnknownExpression;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Field\AbstractProperty;
use JSONAPI\Expression\Fn\Method;
use JSONAPI\Expression\Literal\AbstractValue;
use JSONAPI\Expression\Math;

/**
 * Class Resolver
 *
 * @package JSONAPI\Expression\Dispatcher
 */
abstract class Resolver implements Dispatcher
{
    use Comparisons;
    use Methods;
    use Arithmetics;

    /**
     * @var Accessor
     */
    private Accessor $accessor;

    /**
     * ClosureResolver constructor.
     *
     * @param Accessor|null $accessor - accessor for object properties
     */
    public function __construct(?Accessor $accessor = null)
    {
        $this->accessor = $accessor ?? new SimpleAccessor();
    }

    /**
     * @return Accessor
     */
    protected function getAccessor(): callable
    {
        return $this->accessor;
    }

    /**
     * @param Expression $expression
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function dispatch(Expression $expression): mixed
    {
        return match (true) {
            $expression instanceof Comparison       => $this->resolveComparison($expression),
            $expression instanceof Method           => $this->resolveMethod($expression),
            $expression instanceof Math             => $this->resolveMath($expression),
            $expression instanceof AbstractProperty => $this->resolveField($expression),
            $expression instanceof AbstractValue    => $this->resolveLiteral($expression),
            default                                 => throw new UnknownExpression(get_class($expression))
        };
    }
}
