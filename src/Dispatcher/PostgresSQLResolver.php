<?php

/**
 * Created by tomas
 * at 14.02.2021 17:50
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Dispatcher;

use JSONAPI\Expression\Expression;

/**
 * Class PostgresSQLResolver
 *
 * @package JSONAPI\Expression\Dispatcher
 * @method string dispatch(Expression $expression)
 */
class PostgresSQLResolver extends SQLResolver
{
    /**
     * @inheritDoc
     */
    protected function resolveMethodConcat(array $args): string
    {
        return join('||', $args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodLength(array $args): string
    {
        return sprintf('char_length(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodIndexOf(array $args): string
    {
        return sprintf('position(%s in %s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSubstring(array $args): string
    {
        if (count($args) == 3) {
            return sprintf('substring(%s from %s::INTEGER for %s::INTEGER)', ...$args);
        }
        return sprintf('substring(%s from %s::INTEGER)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMatchesPattern(array $args): string
    {
        return sprintf('%s SIMILAR TO %s', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDate(array $args): string
    {
        return sprintf('EXTRACT(\'date\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTime(array $args): string
    {
        return sprintf('EXTRACT(\'time\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDay(array $args): string
    {
        return sprintf('EXTRACT(\'day\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMonth(array $args): string
    {
        return sprintf('EXTRACT(\'month\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodYear(array $args): string
    {
        return sprintf('EXTRACT(\'year\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodHour(array $args): string
    {
        return sprintf('EXTRACT(\'hour\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMinute(array $args): string
    {
        return sprintf('EXTRACT(\'minute\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSecond(array $args): string
    {
        return sprintf('EXTRACT(\'second\' FROM %s::TIMESTAMP)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodFloor(array $args): string
    {
        return sprintf('FLOOR(%s)', ...$args);
    }


    /**
     * @inheritDoc
     */
    protected function resolveMethodCeiling(array $args): string
    {
        return sprintf('CEIL(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathAddition(mixed $left, mixed $right): string
    {
        return '(' . $left . '::DECIMAL + ' . $right . '::DECIMAL)';
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathSubtraction(mixed $left, mixed $right): string
    {
        return '(' . $left . '::DECIMAL - ' . $right . '::DECIMAL)';
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathModulo(mixed $left, mixed $right): string
    {
        return '(' . $left . '::DECIMAL % ' . $right . '::DECIMAL)';
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathDivision(mixed $left, mixed $right): string
    {
        return '(' . $left . '::DECIMAL / ' . $right . '::DECIMAL)';
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathMultiplication(mixed $left, mixed $right): string
    {
        return '(' . $left . '::DECIMAL * ' . $right . '::DECIMAL)';
    }
}
