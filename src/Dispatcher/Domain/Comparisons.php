<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Dispatcher\Domain;

use JSONAPI\Expression\Comparison;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Exception\UnknownComparisonOperator;
use JSONAPI\Expression\Operator;

trait Comparisons
{
    /**
     * @param Comparison $expression
     *
     * @return mixed
     * @throws ExpressionError
     * @throws UnknownComparisonOperator
     */
    private function resolveComparison(Comparison $expression): mixed
    {
        $left  = $expression->getLeft()->resolve($this);
        $right = $expression->getRight()->resolve($this);
        return match ($expression->getOp()) {
            Operator::LOGICAL_EQUAL                 => $this->resolveComparisonEqual($left, $right),
            Operator::LOGICAL_NOT_EQUAL             => $this->resolveComparisonNotEqual($left, $right),
            Operator::LOGICAL_LOWER_THAN            => $this->resolveComparisonLowerThan($left, $right),
            Operator::LOGICAL_LOWER_THAN_OR_EQUAL   => $this->resolveComparisonLowerThanOrEqual($left, $right),
            Operator::LOGICAL_GREATER_THAN          => $this->resolveComparisonGreaterThan($left, $right),
            Operator::LOGICAL_GREATER_THAN_OR_EQUAL => $this->resolveComparisonGreaterThanOrEqual($left, $right),
            Operator::LOGICAL_IN                    => $this->resolveComparisonIn($left, $right),
            Operator::LOGICAL_HAS                   => $this->resolveComparisonHas($left, $right),
            Operator::LOGICAL_OR                    => $this->resolveComparisonOr($left, $right),
            Operator::LOGICAL_AND                   => $this->resolveComparisonAnd($left, $right),
            Operator::LOGICAL_BETWEEN               => $this->resolveComparisonBetween($left, $right),
            default                                 => throw new UnknownComparisonOperator($expression->getOp())
        };
    }

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonNotEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonLowerThan(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonLowerThanOrEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonGreaterThan(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonGreaterThanOrEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonIn(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonHas(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonOr(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonAnd(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionError
     */
    abstract protected function resolveComparisonBetween(mixed $left, mixed $right): mixed;
}
