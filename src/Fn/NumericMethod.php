<?php

/**
 * Created by tomas
 * 13.06.2022 22:02
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Fn;

use JSONAPI\Expression\Comparability\NumericCompare;
use JSONAPI\Expression\Type\TNumeric;

class NumericMethod extends Method implements TNumeric
{
    use NumericCompare;
}
