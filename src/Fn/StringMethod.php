<?php

/**
 * Created by tomas
 * 13.06.2022 21:57
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Fn;

use JSONAPI\Expression\Comparability\StringCompare;
use JSONAPI\Expression\Type\TString;

class StringMethod extends Method implements TString
{
    use StringCompare;
}
