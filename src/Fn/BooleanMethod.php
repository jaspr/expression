<?php

/**
 * Created by tomas
 * 13.06.2022 22:02
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Fn;

use JSONAPI\Expression\Comparability\BooleanCompare;
use JSONAPI\Expression\Type\TBoolean;

class BooleanMethod extends Method implements TBoolean
{
    use BooleanCompare;
}
