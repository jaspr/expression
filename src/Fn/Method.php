<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Fn;

use JSONAPI\Expression\Dispatcher;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Functional;
use JSONAPI\Expression\Operator;

/**
 * Class Method
 *
 * @package JSONAPI\Expression\Expression
 */
abstract class Method implements Functional
{
    /**
     * @var Operator
     */
    protected Operator $fn;
    /**
     * @var Expression[]
     */
    protected array $args;

    /**
     * Func constructor.
     *
     * @param Operator $func
     * @param array  $args
     */
    public function __construct(Operator $func, array $args)
    {
        $this->fn   = $func;
        $this->args = $args;
    }

    /**
     * @return Operator
     */
    public function getFn(): Operator
    {
        return $this->fn;
    }

    /**
     * @return Expression[]
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
