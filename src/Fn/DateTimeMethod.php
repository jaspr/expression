<?php

/**
 * Created by tomas
 * 13.06.2022 21:59
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Fn;

use JSONAPI\Expression\Comparability\DateTimeCompare;
use JSONAPI\Expression\Type\TDateTime;

class DateTimeMethod extends Method implements TDateTime
{
    use DateTimeCompare;
}
