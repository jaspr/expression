<?php

declare(strict_types=1);

namespace JSONAPI\Expression;

/**
 * Interface Functional
 *
 * @package JSONAPI\Expression\Expression
 */
interface Functional extends Expression
{
    /**
     * @return Operator
     */
    public function getFn(): Operator;


    /**
     * @return Expression[]
     */
    public function getArgs(): array;
}
