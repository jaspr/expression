<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Type;

use JSONAPI\Expression\Expression;

/**
 * Interface TString
 *
 * @package JSONAPI\Expression\Expression
 */
interface TString extends Expression
{
}
