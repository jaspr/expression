<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Type;

use JSONAPI\Expression\Expression;

/**
 * Interface TDateTime
 *
 * @package JSONAPI\Expression\Expression
 */
interface TDateTime extends Expression
{
}
