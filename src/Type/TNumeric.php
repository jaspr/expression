<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Type;

use JSONAPI\Expression\Expression;

/**
 * Interface TNumeric
 *
 * @package JSONAPI\Expression\Expression
 */
interface TNumeric extends Expression
{
}
