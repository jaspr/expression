<?php

/**
 * Created by tomas
 * 12.06.2022 23:33
 */

declare(strict_types=1);

namespace JSONAPI\Expression\Type;

use JSONAPI\Expression\Expression;

interface TArray extends Expression
{
}
