<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Type;

use JSONAPI\Expression\Expression;

/**
 * Interface TBoolean
 *
 * @package JSONAPI\Expression\Expression
 */
interface TBoolean extends Expression
{
}
