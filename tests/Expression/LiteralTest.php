<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Expression;

use JSONAPI\Expression\Literal\ArrayValue;
use JSONAPI\Expression\Literal\NumericValue;
use PHPUnit\Framework\TestCase;

class LiteralTest extends TestCase
{
    public function testArrayValue()
    {
        $value1 = [1, 2, 3];
        $value2 = [new NumericValue(1), new NumericValue(2), new NumericValue(3)];
        $l1     = new ArrayValue($value1);
        $this->assertEquals($value2, $l1->getValue());
        $l2 = new ArrayValue($value2);
        $this->assertEquals($value2, $l2->getValue());
    }
}
