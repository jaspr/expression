<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Dispatcher;

use DateTime;
use Doctrine\ORM\Query\Expr;
use JSONAPI\Expression\Dispatcher\DoctrineQueryResolver;
use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Exception\NotImplemented;
use JSONAPI\Expression\Literal\DateTimeValue;
use JSONAPI\Expression\Literal\NumericValue;
use JSONAPI\Expression\Literal\StringValue;
use PHPUnit\Framework\TestCase;

class DoctrineQueryResolverTest extends TestCase
{
    public function testResolveComparisonEqual()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::eq(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field = 'value'", (string)$filter);
    }

    public function testResolveComparisonLowerThanOrEqual()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::le(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field <= 'value'", (string)$filter);
    }

    public function testResolveMethodTrim()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::trim(Ex::field('field'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("TRIM(field)", (string)$filter);
    }

    public function testResolveMethodContains()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::contains(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field LIKE '%value%'", (string)$filter);
    }

    public function testResolveMethodMonth()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::month(Ex::field('field', 'datetime'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonIn()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::in(Ex::field('field'), Ex::literal(['value']));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field IN('value')", (string)$filter);
    }

    public function testResolveMethodConcat()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::concat(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("CONCAT(field, 'value')", (string)$filter);
    }

    public function testResolveMethodMatchesPattern()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::matchesPattern(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
    }

    public function testResolveMethodSubstring()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::substring(Ex::field('field'), Ex::literal(1), Ex::literal(2));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("SUBSTRING(field, 1, 2)", (string)$filter);
    }

    public function testResolveComparisonGreaterThanOrEqual()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::ge(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field >= 'value'", (string)$filter);
    }

    public function testResolveMethodFloor()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::floor(Ex::field('field', 'int'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonAnd()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::and(
            Ex::ge(Ex::field('field'), Ex::literal('value')),
            Ex::in(Ex::field('field'), Ex::literal(['value']))
        );
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field >= 'value' AND field IN('value')", (string)$filter);
    }

    public function testResolveMethodEndsWith()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::endsWith(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field LIKE '%value'", (string)$filter);
    }

    public function testResolveMethodIndexOf()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::indexOf(Ex::field('field'), Ex::literal('value'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonOr()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::or(
            Ex::ge(Ex::field('field'), Ex::literal('value')),
            Ex::in(Ex::field('field'), Ex::literal(['value']))
        );
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field >= 'value' OR field IN('value')", (string)$filter);
    }

    public function testResolveMethodTime()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::time(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonHas()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::has(Ex::field('field', 'string[]'), Ex::literal('value'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMethodRound()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::round(Ex::field('field', 'int'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonLowerThan()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::lt(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field < 'value'", (string)$filter);
    }

    public function testResolveMethodToUpper()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::toUpper(Ex::field('field'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("UPPER(field)", (string)$filter);
    }

    public function testResolveMethodNot()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::not(Ex::eq(Ex::field('field'), Ex::literal('value')));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("NOT(field = 'value')", (string)$filter);
    }

    public function testResolveComparisonGreaterThan()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::gt(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field > 'value'", (string)$filter);
    }

    public function testResolveMethodCeiling()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::ceiling(Ex::field('field', 'int'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMathAddition()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::add(Ex::field('field', 'int'), Ex::literal(3));
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field + 3", (string)$filter);
    }

    public function testResolveMathSubtraction()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::sub(Ex::field('field', 'int'), Ex::literal(3));
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field - 3", (string)$filter);
    }

    public function testResolveMethodStartsWith()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::startsWith(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field LIKE 'value%'", (string)$filter);
    }

    public function testResolveMathDivision()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::div(Ex::field('field', 'int'), Ex::literal(3));
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field / 3", (string)$filter);
    }

    public function testResolveMethodDate()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::date(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMethodSecond()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::second(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMathMultiplication()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::mul(Ex::field('field', 'int'), Ex::literal(3));
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field * 3", (string)$filter);
    }

    public function testResolveLiteral()
    {
        $dispatcher = new DoctrineQueryResolver();
        $string = new StringValue('string');
        $value = $dispatcher->dispatch($string);
        $this->assertEquals("'string'", $value->__toString());
        $integer = new NumericValue(1);
        $value = $dispatcher->dispatch($integer);
        $this->assertEquals("1", $value->__toString());
        $datetime = new DateTimeValue(new DateTime('2020-01-01', new \DateTimeZone('Europe/Prague')));
        $value = $dispatcher->dispatch($datetime);
        $this->assertEquals("'2020-01-01T00:00:00+01:00'", $value->__toString());
    }

    public function testResolveMethodLength()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::length(Ex::field('field'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("LENGTH(field)", (string)$filter);
    }

    public function testResolveMethodHour()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::hour(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMethodToLower()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::toLower(Ex::field('field'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("LOWER(field)", (string)$filter);
    }

    public function testResolveMethodMinute()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::minute(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMethodDay()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::day(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }

    public function testResolveMathModulo()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::mod(Ex::field('field', 'int'), Ex::literal(3));
        $dispatcher->dispatch($ex);
    }

    public function testResolveComparisonBetween()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::be(Ex::field('field', 'int'), Ex::literal(1), Ex::literal(3));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field BETWEEN 1 AND 3", (string)$filter);
    }

    public function testResolveComparisonNotEqual()
    {
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::ne(Ex::field('field'), Ex::literal('value'));
        /** @var Expr\Comparison $filter */
        $filter = $dispatcher->dispatch($ex);
        $this->assertEquals("field <> 'value'", (string)$filter);
    }

    public function testResolveMethodYear()
    {
        $this->expectException(NotImplemented::class);
        $dispatcher = new DoctrineQueryResolver();
        $ex = Ex::year(Ex::field('field', 'datetime'));
        $dispatcher->dispatch($ex);
    }
}
