<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Dispatcher;

use DateTime;
use JSONAPI\Expression\Dispatcher;
use JSONAPI\Expression\Dispatcher\ClosureResolver;
use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Exception\UnknownExpression;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Literal\ArrayValue;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class ClosureResolverTest
 *
 * @package JSONAPI\Expression\Test\Dispatcher
 */
class ClosureResolverTest extends TestCase
{
    public function testUnknownExpression()
    {
        $this->expectException(UnknownExpression::class);
        $dispatcher = new ClosureResolver();
        $expression = new class implements Expression {
            /**
             * @param Dispatcher $dispatcher
             *
             * @return mixed
             */
            public function resolve(Dispatcher $dispatcher): mixed
            {
                return $dispatcher->dispatch($this);
            }

            public function isComparable(Expression $expression): bool
            {
                return true;
            }
        };
        $expression->resolve($dispatcher);
    }

    public function testComparison()
    {
        $obj1        = new stdClass();
        $obj1->field = 1;
        $obj1->arr   = [4, 5, 6];
        $obj2        = new stdClass();
        $obj2->field = 2;
        $obj2->arr   = [1, 2, 3];
        $arr         = [$obj1, $obj2];

        $expression = Ex::eq(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj1, $result);

        $expression = Ex::ne(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj2, $result);

        $expression = Ex::gt(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj2, $result);

        $expression = Ex::ge(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::lt(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertEmpty($result);

        $expression = Ex::le(Ex::field('field', 'integer'), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj1, $result);

        $expression = Ex::in(Ex::field('field', 'integer'), new ArrayValue([1, 2]));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::and(
            Ex::eq(Ex::field('field', 'integer'), Ex::literal(1)),
            Ex::in(Ex::field('field', 'integer'), new ArrayValue([1, 2]))
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj1, $result);

        $expression = Ex::or(
            Ex::eq(Ex::field('field', 'integer'), Ex::literal(1)),
            Ex::in(Ex::field('field', 'integer'), new ArrayValue([1, 2]))
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertContains($obj2, $result);
        $this->assertContains($obj1, $result);
    }

    public function testArithmetic()
    {
        $obj1        = new stdClass();
        $obj1->field = 1;
        $obj2        = new stdClass();
        $obj2->field = 2;
        $arr         = [$obj1, $obj2];

        $expression = Ex::eq(
            Ex::add(Ex::literal(8), Ex::literal(2)),
            Ex::literal(10)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::sub(Ex::literal(8), Ex::literal(2)),
            Ex::literal(6)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::mul(Ex::literal(8), Ex::literal(2)),
            Ex::literal(16)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::div(Ex::literal(8), Ex::literal(2)),
            Ex::literal(4)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::mod(Ex::literal(8), Ex::literal(2)),
            Ex::literal(0)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);
    }

    public function testFunction()
    {
        $obj1        = new stdClass();
        $obj1->field = 1;
        $obj2        = new stdClass();
        $obj2->field = 2;
        $arr         = [$obj1, $obj2];

        $expression = Ex::eq(Ex::length(Ex::literal('test')), Ex::literal(4));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::contains(Ex::literal('test'), Ex::literal('s'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::concat(Ex::literal('test'), Ex::literal('s')), Ex::literal('tests'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::endsWith(Ex::literal('test'), Ex::literal('t'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::startsWith(Ex::literal('test'), Ex::literal('t'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::not(Ex::startsWith(Ex::literal('test'), Ex::literal('s')));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::trim(Ex::literal('    test   ')), Ex::literal('test'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::indexOf(Ex::literal('test'), Ex::literal('es')), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::substring(Ex::literal('test'), Ex::literal(1), Ex::literal(1)), Ex::literal('e'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::matchesPattern(Ex::literal('test'), Ex::literal('/^t.*t$/'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::toLower(Ex::literal('TEST')), Ex::literal('test'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::toUpper(Ex::literal('test')), Ex::literal('TEST'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::toUpper(Ex::literal('test')), Ex::literal('TEST'));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::date(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::date(Ex::literal(new DateTime('2020-12-31 23:59:59')))
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::year(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(2020)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::month(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(12)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::day(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(31)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::hour(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(15)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::minute(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(30)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::second(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::literal(45)
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(
            Ex::time(Ex::literal(new DateTime('2020-12-31 15:30:45'))),
            Ex::time(Ex::literal(new DateTime('15:30:45')))
        );
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::ceiling(Ex::literal(1.2)), Ex::literal(2));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::floor(Ex::literal(1.2)), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);

        $expression = Ex::eq(Ex::round(Ex::literal(1.2)), Ex::literal(1));
        $filter     = $expression->resolve(new ClosureResolver());
        $result     = array_filter($arr, $filter);
        $this->assertCount(2, $result);
    }
}
