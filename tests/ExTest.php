<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test;

use DateTime;
use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Comparison;
use JSONAPI\Expression\Fn\Method;
use JSONAPI\Expression\Math;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;
use PHPUnit\Framework\TestCase;

class ExTest extends TestCase
{
    public function testStartsWith()
    {
        $ex = Ex::startsWith(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testEq()
    {
        $ex = Ex::eq(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testNe()
    {
        $ex = Ex::ne(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testMul()
    {
        $ex = Ex::mul(Ex::literal(1), Ex::literal(1));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testOr()
    {
        $ex = Ex::or(
            Ex::ne(Ex::literal(''), Ex::literal('')),
            Ex::eq(Ex::literal(''), Ex::literal(''))
        );
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testTrim()
    {
        $ex = Ex::trim(Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testNot()
    {
        $ex = Ex::not(Ex::literal(true));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testConcat()
    {
        $ex = Ex::concat(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testIndexOf()
    {
        $ex = Ex::indexOf(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testDate()
    {
        $ex = Ex::date(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TDateTime::class, $ex);
    }

    public function testSecond()
    {
        $ex = Ex::second(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMod()
    {
        $ex = Ex::mod(Ex::literal(1), Ex::literal(1));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testSub()
    {
        $ex = Ex::sub(Ex::literal(1), Ex::literal(1));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testDay()
    {
        $ex = Ex::day(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLiteral()
    {
        $ex = Ex::literal('');
        $this->assertInstanceOf(TString::class, $ex);
        $ex = Ex::literal(1);
        $this->assertInstanceOf(TNumeric::class, $ex);
        $ex = Ex::literal(false);
        $this->assertInstanceOf(TBoolean::class, $ex);
        $ex = Ex::literal([]);
        $this->assertInstanceOf(TArray::class, $ex);
        $ex = Ex::literal(new DateTime());
        $this->assertInstanceOf(TDateTime::class, $ex);
    }

    public function testLt()
    {
        $ex = Ex::lt(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testContains()
    {
        $ex = Ex::contains(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testEndsWith()
    {
        $ex = Ex::endsWith(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testAdd()
    {
        $ex = Ex::add(Ex::literal(1), Ex::literal(2));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMinute()
    {
        $ex = Ex::minute(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testHas()
    {
        $ex = Ex::has(Ex::literal([]), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testTime()
    {
        $ex = Ex::time(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TDateTime::class, $ex);
    }

    public function testDiv()
    {
        $ex = Ex::div(Ex::literal(1), Ex::literal(1));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testField()
    {
        $ex = Ex::field('', 'string');
        $this->assertInstanceOf(TString::class, $ex);
        $ex = Ex::field('', 'integer');
        $this->assertInstanceOf(TNumeric::class, $ex);
        $ex = Ex::field('', 'boolean');
        $this->assertInstanceOf(TBoolean::class, $ex);
        $ex = Ex::field('', 'double');
        $this->assertInstanceOf(TNumeric::class, $ex);
        $ex = Ex::field('', 'datetime');
        $this->assertInstanceOf(TDateTime::class, $ex);
        $ex = Ex::field('', 'string[]');
        $this->assertInstanceOf(TArray::class, $ex);
        $ex = Ex::field('', 'integer[]');
        $this->assertInstanceOf(TArray::class, $ex);
        $ex = Ex::field('', 'boolean[]');
        $this->assertInstanceOf(TArray::class, $ex);
        $ex = Ex::field('', 'double[]');
        $this->assertInstanceOf(TArray::class, $ex);
        $ex = Ex::field('', 'datetime[]');
        $this->assertInstanceOf(TArray::class, $ex);
    }

    public function testSubstring()
    {
        $ex = Ex::substring(Ex::literal(''), Ex::literal(1), Ex::literal(2));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testGe()
    {
        $ex = Ex::ge(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testMatchesPattern()
    {
        $ex = Ex::matchesPattern(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testHour()
    {
        $ex = Ex::hour(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testFloor()
    {
        $ex = Ex::floor(Ex::literal(1));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLength()
    {
        $ex = Ex::length(Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMonth()
    {
        $ex = Ex::month(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testCeiling()
    {
        $ex = Ex::ceiling(Ex::literal(1));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testToLower()
    {
        $ex = Ex::toLower(Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testAnd()
    {
        $ex = Ex::and(
            Ex::lt(Ex::literal(1), Ex::literal(2)),
            Ex::gt(Ex::literal(1), Ex::literal(2))
        );
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testRound()
    {
        $ex = Ex::round(Ex::literal(1));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLe()
    {
        $ex = Ex::le(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testGt()
    {
        $ex = Ex::gt(Ex::literal(''), Ex::literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testYear()
    {
        $ex = Ex::year(Ex::literal(new DateTime()));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testIn()
    {
        $ex = Ex::in(Ex::literal(1), Ex::literal([1, 2]));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testToUpper()
    {
        $ex = Ex::toUpper(Ex::literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }
}
