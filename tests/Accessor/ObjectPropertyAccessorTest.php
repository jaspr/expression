<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Accessor;

use JSONAPI\Expression\Accessor\ObjectPropertyAccessor;
use JSONAPI\Expression\Field\NumericProperty;
use JSONAPI\Expression\Field\StringProperty;
use JSONAPI\Expression\Test\Resource\ObjectOne;
use PHPUnit\Framework\TestCase;
use TypeError;

/**
 * Class ObjectPropertyAccessorTest
 *
 * @package JSONAPI\Expression\Test\Accessor
 */
class ObjectPropertyAccessorTest extends TestCase
{
    public function testFieldAccess()
    {
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals('private', $accessor(new StringProperty('priProp'), [$object]));
        $this->assertEquals('protected', $accessor(new StringProperty('proProp'), [$object]));
        $this->assertEquals('public', $accessor(new StringProperty('pubProp'), [$object]));
    }

    public function testDeepAccess()
    {
        $field    = new NumericProperty('two.intProp');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals(1234, $accessor($field, [$object]));

        $field    = new NumericProperty('two.three.intProp');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals(1234, $accessor($field, [$object]));
    }

    public function testNonExistingProperty()
    {
        $this->expectException(TypeError::class);
        $field    = new StringProperty('two.three.intProp.nonExist');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();
        $accessor($field, [$object]);
    }
}
