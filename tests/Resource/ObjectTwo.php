<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Resource;

class ObjectTwo
{
    private int $intProp = 1234;

    private string $strProp = 'string';

    private bool $boolProp = true;

    private float $floatProp = 12.34;

    private mixed $nullProp = null;

    private array $arrProp = [1, 2, 3, 4];

    private ObjectThree $three;

    /**
     * ObjectTwo constructor.
     */
    public function __construct()
    {
        $this->three = new ObjectThree();
    }


    /**
     * @return int
     */
    public function getIntProp(): int
    {
        return $this->intProp;
    }

    /**
     * @return string
     */
    public function getStrProp(): string
    {
        return $this->strProp;
    }

    /**
     * @return bool
     */
    public function isBoolProp(): bool
    {
        return $this->boolProp;
    }

    /**
     * @return float
     */
    public function getFloatProp(): float
    {
        return $this->floatProp;
    }

    /**
     * @return null
     */
    public function getNullProp(): mixed
    {
        return $this->nullProp;
    }

    /**
     * @return array|int[]
     */
    public function getArrProp(): array
    {
        return $this->arrProp;
    }

    /**
     * @return ObjectThree
     */
    public function getThree(): ObjectThree
    {
        return $this->three;
    }
}
