<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Resource;

class ObjectOne
{
    public string $pubProp = 'public';
    protected string $proProp = 'protected';
    private string $priProp = 'private';
    private ObjectTwo $two;

    public function __construct()
    {
        $this->two = new ObjectTwo();
    }

    /**
     * @return string
     */
    public function getPriProp(): string
    {
        return $this->priProp;
    }

    /**
     * @return string
     */
    public function getProProp(): string
    {
        return $this->proProp;
    }

    /**
     * @return ObjectTwo
     */
    public function getTwo(): ObjectTwo
    {
        return $this->two;
    }
}
