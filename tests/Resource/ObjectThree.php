<?php

declare(strict_types=1);

namespace JSONAPI\Expression\Test\Resource;

class ObjectThree
{
    public int $intProp = 1234;

    public string $strProp = 'string';

    public bool $boolProp = true;

    public float $floatProp = 12.34;

    public mixed $nullProp = null;

    public array $arrProp = [1, 2, 3, 4];
}
