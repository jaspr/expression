# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.4](https://gitlab.com/jaspr/expression/compare/1.1.3...1.1.4) (2024-12-07)


### Fixed

* fix DoctrineCriteriaResolver ([12e90ea](https://gitlab.com/jaspr/expression/commit/12e90ea5cbe1c7e6d7550d7b37a9ffeab6f6fb07))
* implicit nullable param definition ([abe9913](https://gitlab.com/jaspr/expression/commit/abe99138ac6965ef0d026c42deec507137b89e3f))


### Changed

* replace generic Exception to RuntimeException ([d990a3a](https://gitlab.com/jaspr/expression/commit/d990a3a2bd5830bd1d61ad8a7b347709583aec24))

## [1.1.3](https://gitlab.com/jaspr/expression/compare/1.1.2...1.1.3) (2023-06-23)


### Fixed

* enum type mapping ([7364a84](https://gitlab.com/jaspr/expression/commit/7364a84bd5c6597b08a00c72052162a412403a0b))

## [1.1.2](https://gitlab.com/jaspr/expression/compare/1.1.1...1.1.2) (2023-03-06)


### Fixed

* fix eq with null ([f2bcceb](https://gitlab.com/jaspr/expression/commit/f2bcceb6d8d8dba8a5901955fd1c6549a9209f1c))

### [1.1.1](https://gitlab.com/jaspr/expression/compare/1.1.0...1.1.1) (2022-12-27)


### Fixed

* Fix DoctrineQueryResolver ([7930f00](https://gitlab.com/jaspr/expression/commit/7930f007980fa006ae710c9db18c6962c3d436aa))

## [1.1.0](https://gitlab.com/jaspr/expression/compare/1.0.0...1.1.0) (2022-07-26)


### Added

* enable mixed array for field ([89d6d5e](https://gitlab.com/jaspr/expression/commit/89d6d5e6f94f0d162b4abe282694045a39ea6195))

## 1.0.0 (2022-06-20)


### Changed

* moving project ([a931189](https://gitlab.com/jaspr/expression/commit/a931189871866ddf2a68288291db0fdcec152a5f))
